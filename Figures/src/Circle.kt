import kotlin.math.PI
// TODO: дополнить определение класса размерами и позицией
class Circle(x: Int, y: Int, name: String, var r: Int) : Figure(x, y, name), Transforming, Movable {
    // TODO: реализовать интерфейс Transforming

    override fun move(dx: Int, dy: Int) {
        x += dx
        y += dy
    }

    override fun resize(zoom: Int) {
        r *= zoom
    }

    override fun rotate(direction: RotateDirection, centerX: Int, centerY: Int) {
        val y1 = y
        x = if (direction == RotateDirection.Clockwise) {
            y = centerY - (x - centerX)
            centerX + (y1 - centerY)
        } else {
            y = centerY + (x - centerX)
            centerX - (y1 - centerY)
        }
    }

    override fun area(): Float {
        return  (PI * r * r).toFloat();
    }

    override fun toString(): String {
        return "Этот $name находится в точке ($x,$y), его радиус - $r"
    }
}