// если хотя бы есть один метод абстрактный (без реализации), класс объявляется абстрактным
abstract class Figure(var x: Int, var y: Int, var name: String) {
    // мы не знаем площади абстрактной фигуры, но это не мешает нам определить метод
     abstract fun area(): Float

//     abstract fun set_name(): String
}